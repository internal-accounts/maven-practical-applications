# Maven Practical Applications
# **NOTE**
## The folder structure has been updated from **sling-multi-module-magic** to **sling-multi-module**


## Training Projects For Maven

### Current Steps
** Walk through Slick 2 Blog Platform
** Data gathering on notes about Sling(frustating to name the least - most of the content is outdated which is a problem because alot of their solutions break)


### Project Issues
* There are way too many **project templates** to consider when starting with an archetype generated via Maven. Rather use option 1212 when starting
* Server issue - some of these projects do not allow you to create a local server variable through Eclipse. This is a notable problem as it might mean that the project has not been setup correctly
```
After speaking to Kabelo, we've noted that it might also be a dependemcy issue
```



## Final Project Breakdown
### Project Objectives
```
25-26 July 2018
```
* Create a simple page with 4 links(menu links); home, music, flowers, dancing.
* Each link must link to individual pages under each section.
* Each page must have an identifier to show that the page has changed successfully(this can be static content in the meantime)

** Development Outcome**
* I have created a new page, named **sitewide** that has the menu page and the child pages under that
* The homepage that contains the menu items can be viewed by going to http://localhost:8080/sitewide/index.html

**Greg's Feedback**
* Feedback goes here

```
30 July - 03 August 2018
```
* Enable the music page to create "track titles" and edit them
* The page should also populate the titles